# syntax=docker/dockerfile:1
FROM ghcr.io/linuxserver/code-server

ENV PYTHONUNBUFFERED=1

# install necessary libraries using apt-get
RUN apt-get update \
  && apt-get install --no-install-recommends  -y \
    bash-completion vim iputils-ping \
    graphviz \
    libpq-dev build-essential \
    python3-venv python3-dev \
    python3.8-venv python3.8-dev \
    # sphinx requirements from https://github.com/sphinx-doc/docker/blob/4.2.0/latexpdf/Dockerfile
    graphviz \
    imagemagick \
    make \
    latexmk \
    lmodern \
    fonts-freefont-otf \
    texlive-latex-recommended \
    texlive-latex-extra \
    texlive-fonts-recommended \
    texlive-fonts-extra \
    texlive-lang-cjk \
    texlive-lang-chinese \
    texlive-lang-japanese \
    texlive-lang-german \
    texlive-luatex \
    texlive-xetex \
    xindy \
    tex-gyre \
  && apt-get autoremove \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# install git-mob
RUN npm i -g git-mob

COPY docker/workspace/config /config
COPY docker/workspace/git-user-management /config/git-user-management
RUN make -C /config/git-user-management install

ADD . /config/workspace_src

ENV PATH="/config/bin:${PATH}"

EXPOSE 8443
