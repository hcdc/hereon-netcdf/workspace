#!/bin/bash
# populate the workspace
#
# This script clones the workspace repository at https://gitlab.hzdr.de/hcdc/hereon-netcdf/workspace.git
# and installs it at /config/workspace, including the python virtual
# environment. Additionally, it starts some static file servers to host the
# documentation. They are accessible with the browser at
#
# /proxy/9001 https://gitlab.hzdr.de/hcdc/hereon-netcdf/hereon-netcdf-en
# /proxy/9002 https://gitlab.hzdr.de/hcdc/hereon-netcdf/hereon-netcdf-de
#
# NOTE: We do not overwrite any existing folder here

set -e
cd /config/workspace

if [[ ! -d .git ]]; then
    cp -r /config/workspace_src/. .
    git submodule update --init
    git remote set-url origin https://gitlab.hzdr.de/hcdc/hereon-netcdf/workspace.git
fi

if [[ ! -d venv ]]; then
    echo "**** setting up python ****"

    PYTHON=python3.8 ./install.sh
else
    echo "**** python already setup. skipping ****"
fi

# build and serve the docs
source venv/bin/activate

# main repo
[ ! -d docs/hereon-netcdf-en/build/html ] && sphinx-build docs/hereon-netcdf-en/source docs/hereon-netcdf-en/build/html
nohup python -m http.server --directory docs/hereon-netcdf-en/build/html 9001 &

# prototype repo
[ ! -d docs/hereon-netcdf-de/build/html ] && sphinx-build docs/hereon-netcdf-de/source docs/hereon-netcdf-de/build/html
nohup python -m http.server --directory docs/hereon-netcdf-de/build/html 9002 &

pip install -e python/sphinxext

chown -R abc: /config/workspace
