#!/bin/bash
# Configure command line git
#
# In this script, we set the global git user.name and user.email, and we add
# create the .git-coauthors file (if not already existing).
#
# We then check for GPG keys in /config/certs and add the corresponding names
# to the .git-coauthors file.
#
# Furthermore we give the user the possibility to specify the global git
# user.name via the DEFAULT_GIT_USER_NAME environment variable, and the global
# git user.email via the DEFAULT_GIT_USER_EMAIL environment variable.

set -e

git config --global commit.gpgsign true

if [[ ! -e ~/users_config/.git-coauthors ]]; then
  mkdir -p ~/users_config
  echo '{"coauthors": {}}' > ~/users_config/.git-coauthors
fi

if [[ ! -e ~/users_config/.gnupg ]]; then
  mkdir -p ~/users_config/.gnupg
fi
chmod 700 ~/users_config/.gnupg

ln -sf ~/users_config/.git-coauthors ~/.git-coauthors
ln -sf ~/users_config/.gnupg ~/.gnupg

git add-coauthors

if [[ ${DEFAULT_GIT_USER_NAME} ]]; then
    git config --global user.name ${DEFAULT_GIT_USER_NAME}
fi

if [[ ${DEFAULT_GIT_USER_EMAIL} ]]; then
    git config --global user.email ${DEFAULT_GIT_USER_EMAIL}
fi

if [ "${DEFAULT_GIT_USER_NAME}" != "" ] && [ "${DEFAULT_GIT_USER_EMAIL}" != "" ]; then
    INITIALS=""
    for NAME in ${DEFAULT_GIT_USER_NAME}; do
        INITIALS="${INITIALS}${NAME:0:1}"
    done
    git add-coauthor ${INITIALS,,} "${DEFAULT_GIT_USER_NAME}" ${DEFAULT_GIT_USER_EMAIL}
fi

chown -R abc: /config/users_config
