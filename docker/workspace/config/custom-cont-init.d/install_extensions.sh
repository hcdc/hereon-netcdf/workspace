#!/bin/bash

code-server --extensions-dir /config/extensions --install-extension ms-python.python
code-server --extensions-dir /config/extensions --install-extension lextudio.restructuredtext
code-server --extensions-dir /config/extensions --install-extension gitlab.gitlab-workflow
code-server --extensions-dir /config/extensions --install-extension njpwerner.autodocstring

if [ ! -z "$(ls -A /config/extensions/*.vsix)" ]; then
  for FNAME in /config/extensions/*.vsix; do
    code-server --extensions-dir /config/extensions --install-extension ${FNAME}
  done
fi
